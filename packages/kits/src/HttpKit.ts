import { AxiosHttpKit } from './AxiosHttpKit'
/**
 * @author Javen
 * @copyright javendev@126.com
 * @description 封装网络请求工具
 */
export class HttpKit {
  private static delegate: HttpDelegate = new AxiosHttpKit()

  public static get getHttpDelegate(): HttpDelegate {
    return this.delegate
  }

  public static set setHttpDelegate(delegate: HttpDelegate) {
    this.delegate = delegate
  }
}

export interface HttpDelegate {
  httpGet(url: string): Promise<any>
  httpGetWitchOptions(url: string, options?: any): Promise<any>
  httpPost(url: string, data: string): Promise<any>
  httpPostWitchOptions(url: string, data: string, options?: any): Promise<any>
  httpPostWithCert(url: string, data: string, certFileContent: Buffer, caFileContent: Buffer, passphrase: string): Promise<any>
  upload(url: string, filePath: string, params?: string): Promise<any>
}
